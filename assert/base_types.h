
#include "os_compiler.h"
#include "platform_attributes.h"

#if !defined(SHIP_MODE)
# define SHIP_MODE 0
#else
# undef  SHIP_MODE
# define SHIP_MODE 1
#endif

#define _NO_CRT_STDIO_INLINE
#include <stdio.h>

#define internal static
#define local_persist static
#define global static

#include <stdint.h>

typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef i32      b32;
typedef float    r32;
typedef double   r64;

#define Statement(s) do { s } while(0)

#define stringify_(s) #s
#define stringify(s) stringify_(s)
#define glue_(a,b) a##b
#define glue(a,b) glue_(a,b)
