
// TODO(sammynilla): May 5, 2020
// Would be nice to add support for more compilers.
// Although, below are the primary compilers that I will be using.
// NOTE(sammynilla): May 5, 2020
// Currently there is support for the following compilers:
// Windows: MSVC
// Linux:   GCC
// MacOS:   Clang

#if defined(_MSC_VER)
# define COMPILER_CL 1
# if defined(_WIN32)
#  define OS_WINDOWS 1
# else
#  error This compiler/platform combo is not supported yet
# endif
#elif defined(__clang__)
# define COMPILER_CLANG 1
# if defined(__APPLE__) && defined(__MACH__)
#  define OS_MAC 1
# else
#  error This compiler/platform combo is not supported yet
# endif
#elif defined(__GNUC__) || defined(__GNUG__)
# define COMPILER_GCC 1
# if defined(__gnu_linux__)
#  define OS_LINUX 1
# else
#  error This compiler/platform combo is not supported yet
# endif
#endif

#if !defined(COMPILER_CL)
#define COMPILER_CL 0
#endif
#if !defined(COMPILER_CLANG)
#define COMPILER_CLANG 0
#endif
#if !defined(COMPILER_GCC)
#define COMPILER_GCC 0
#endif
#if !defined(OS_WINDOWS)
#define OS_WINDOWS 0
#endif
#if !defined(OS_MAC)
#define OS_MAC 0
#endif
#if !defined(OS_LINUX)
#define OS_LINUX 0
#endif
