
// NOTE(sammynilla): May 8, 2020
// This is subject to be added to over time, but for now this is our current use case.

#if defined(__has_cpp_attribute)
# define MY_CPP_ATTRIBUTE(a) __has_cpp_attribute(a)
#else
# define MY_CPP_ATTRIBUTE(a) (0)
#endif

#if COMPILER_CL
# define UNUSED_DEFINITION
# if MY_CPP_ATTRIBUTE(maybe_unused)
#  undef UNUSED_DEFINITION
#  define UNUSED_DEFINITION [[maybe_unused]]
# endif
#elif COMPILER_GCC
# define UNUSED_DEFINITION __attribute((unused))
#endif
