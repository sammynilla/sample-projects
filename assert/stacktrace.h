
#if COMPILER_CL
# if !MY_CPP_ATTRIBUTE(maybe_unused)
#  define PPPragma(x) __pragma(x)
#  define PPPush      PPPragma(warning(push))
#  define PPPop       PPPragma(warning(pop))
#  define PPUnusedVar PPPragma(warning(disable:4189))
# endif
#elif COMPILER_GCC
# if (__GNUC__ >= 9)
#  define PPPragma(x) _Pragma(stringify(x))
#  define PPPush      PPPragma(GCC diagnostic push)
#  define PPPop       PPPragma(GCC diagnostic pop)
#  define PPNarrowing PPPragma(GCC diagnostic ignored "-Wnarrowing")
# endif
#elif COMPILER_CLANG
// TODO(sammynilla): May 8, 2020
// Implement this functionality under clang compiler
#endif

// NOTE(sammynilla): May 8, 2020
// Zero/Empty out PPPragma commands
#if !defined(PPPush)
# define PPPush
#endif
#if !defined(PPPop)
# define PPPop
#endif
#if !defined(PPUnusedVar)
# define PPUnusedVar
#endif
#if !defined(PPNarrowing)
# define PPNarrowing
#endif

// NOTE(sammynilla): May 4, 2020
// Some various good resources for this type of information.
// https://jonjagger.blogspot.com/2017/07/compile-time-assertions-in-c.html
// https://en.wikipedia.org/wiki/Assertion_(software_development)
// https://www.pixelbeat.org/programming/gcc/static_assert.html

#define AssertBreak(Predicate) (*((i32*)0) = 0)
#define AssertCheck(Predicate) \
Statement( if (!(Predicate)) { AssertBreak(Predicate); } )

// NOTE(sammynilla): May 4, 2020
// We use -1 as our false flag subscript because some compilers are capable of supporting 0 size arrays.
// {'!'} is our initialization byte, and will cause any other compilers to complain.
#define RawStaticAssertCheck(Predicate, Message, Alias) \
UNUSED_DEFINITION \
char glue(glue(__sa_chk_,__LINE__), \
glue(glue(_, Alias), __))[(Predicate) ? 1 : -1] = {'!'};

#if defined(DEACTIVATE_SA_CHK_WARNINGS) || defined(REACTIVATE_SA_CHK_WARNINGS)
# undef DEACTIVATE_SA_CHK_WARNINGS
# undef REACTIVATE_SA_CHK_WARNINGS
#endif

#define DEACTIVATE_SA_CHK_WARNINGS PPPush PPUnusedVar PPNarrowing
#define REACTIVATE_SA_CHK_WARNINGS PPPop

#define SanitizedStaticAssertCheck(Predicate, Message, Alias) \
DEACTIVATE_SA_CHK_WARNINGS \
RawStaticAssertCheck(Predicate, Message, Alias) \
REACTIVATE_SA_CHK_WARNINGS

#if !SHIP_MODE
# define Assert(Predicate) AssertCheck(Predicate)
// NOTE(sammynilla): May 6, 2020
// __COUNTER__ allows us to bypass having to manually specify a unique value that would otherwise prevent the user from applying StaticAsserts on the same line across alternative compilation units.
# if defined(__COUNTER__)
#  define StaticAssert(Predicate, Message) \
SanitizedStaticAssertCheck(Predicate, Message, __COUNTER__)
# else
#  define StaticAssert(Predicate, Message) \
SanitizedStaticAssertCheck(Predicate, Message, default)
#endif
// NOTE(sammynilla): May 6, 2020
// StaticAssertEdgeCase() is only really ever needed in the event that you are using a compiler that does not have support for __COUNTER__
# define StaticAssertEdgeCase(Predicate, Message, Alias) \
SanitizedStaticAssertCheck(Predicate, Message, Alias)
#else
# define Assert(Predicate)
# define StaticAssert(Predicate, Message)
# define StaticAssertEdgeCase(Predicate, Message, Alias)
#endif